{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Welcome to CoBRA\n",
    "\n",
    "This tutorial will show you the basic steps to start with [CoBRA](https://cobra.cps.cit.tum.de/), the **Co**llaborative **B**enchmark for **R**obotics **A**pplications.\n",
    "We will show you the following:\n",
    "\n",
    "* How to download tasks and their visualizations from CoBRA.\n",
    "* How to retrieve models of Modular Robots.\n",
    "* How to create and upload a solution.\n",
    "\n",
    "To highlight CoBRA's open standard, we will rely on no external libraries for this first tutorial. However, we would like to emphasize that a great integration with [Timor Python](https://pypi.org/project/timor-python/) exists, which is discussed in the [following tutorial](./1_CoBRA_Timor_Interaction.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Install CoBRA IO\n",
    "!pip install cobra-io"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Retrieve Task Files and Assets\n",
    "\n",
    "First, we show how to find and download tasks via the CoBRA API using CoBRA IO. Under the hood, it uses the API found at [cobra.cps.cit.tum.de/api/](https://cobra.cps.cit.tum.de/api/). Please look at the [task tutorial](Task_Details.ipynb) to understand and edit tasks more precisely."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cobra.task\n",
    "task_uuids, task_details = cobra.task.find_tasks(id=\"simple/PTP_3\")\n",
    "print(f\"Found {len(task_uuids)} tasks\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this, we have found a specific task in the CoBRA database. We can now download the task files and assets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "task_file = cobra.task.get_task(uuid=task_uuids[0])\n",
    "print(f\"The task is located at: {task_file}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The task details allow us to show the stored visualization from the CoBRA website. It shows the task's three goal poses (note goals 1 and 3 overlap) and the base constraint. In the background, it renders the silhouette of the CNC machine the robot should load pieces into in red."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display task\n",
    "from colab_utils import display_cobra\n",
    "display_cobra(task_details[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating a Robot to Solve the Task\n",
    "Within this task, a robot should operate. CoBRA provides a multitude of robots. Here we will show you how to get the URDF of a specific modular robot. You could use this URDF file in any robot simulation, such as [Pinocchio](https://github.com/stack-of-tasks/pinocchio), [peter corke's robot toolbox](https://github.com/petercorke/robotics-toolbox-python), [Timor Python](https://pypi.org/project/timor-python/), etc. If you want to alter or change this robot, look at the [robot tutorial](Robot_Details.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cobra.robot\n",
    "module_db_name = 'modrob-gen2'\n",
    "module_order = ['105', '2', '2', '24', '2', '25', '1', '1', '1', 'GEP2010IL']\n",
    "urdf_file = cobra.robot.get_urdf(module_db_name, module_order)\n",
    "print(\"URDF file at: \", urdf_file)\n",
    "print(\"URDF file assets are stored at: \", urdf_file.parent)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating a Solution\n",
    "We will now create a solution to the given task. One could implement this, e.g., by solving the IK problem for each task pose as done in the [following tutorial](./1_CoBRA_Timor_Interaction.ipynb). Within this basic tutorial, we will adapt a solution already submitted to the benchmark."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cobra.solution\n",
    "solution_uuids, solution_details = cobra.solution.find_solutions(task_uuid=task_uuids[0])\n",
    "print(f\"Found {len(solution_uuids)} solutions for task {task_details[0]['scenario_id']}\")\n",
    "display_cobra(solution_details[0])\n",
    "solution_file, _ = cobra.solution.get_solution(solution_uuids[0])  # Returns the associated task file as well"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now adapt the JSON object to package this solution with your contact information. If you want to edit solutions more precisely, refer to the [solution tutorial](Solution_Details.ipynb); an easier solution creation is discussed in the [next tutorial](./1_CoBRA_Timor_Interaction.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "solution_data = json.load(solution_file.open())\n",
    "\n",
    "solution_data['author'] = [\"your-name\"]\n",
    "solution_data['email'] = [\"your-email\"]\n",
    "solution_data['affiliation'] = [\"your-affiliation\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Submit Solution\n",
    "Finally, let us submit the found solution. [Register an account](https://cobra.cps.cit.tum.de/signup) and use your registration E-Mail in the solution created in the previous section."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cobra.solution\n",
    "import tempfile\n",
    "\n",
    "with tempfile.NamedTemporaryFile(mode=\"w\", suffix=\".json\") as new_solution_file:\n",
    "    json.dump(solution_data, new_solution_file)\n",
    "    new_solution_file.flush()\n",
    "    submission_success, submission_uuid = cobra.solution.submit_solution(new_solution_file.name, solution_data['email'][0])\n",
    "if submission_success:\n",
    "    print(\"Submitted solution successfully. You can find it at cobra.cps.cit.tum.de/user/submissions\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After submission, we can also get the solution again to find its verdict and visualize it:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solution_details = cobra.solution.get_solution_details(submission_uuid)\n",
    "print(\"The uploaded solution is valid: \", solution_details['valid'])\n",
    "display_cobra(solution_details)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conclusion\n",
    "\n",
    "Thank you for being so interested in the CoBRA benchmarking suite. This first tutorial covered how to interact with the CoBRA API to retrieve robot models and tasks and how to create a solution to a simple task and upload it.\n",
    "\n",
    "As the following steps, we suggest looking into the advanced tutorial in this folder or on [CoBRA](https://gitlab.lrz.de/tum-cps/cobra-io/-/tree/main/tutorials)  to take a closer look at each element of the CoBRA benchmark suite."
   ]
  }
 ],
 "metadata": {
  "colab": {
   "authorship_tag": "ABX9TyNj2MscGTurgfvqO/2BuG7k",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

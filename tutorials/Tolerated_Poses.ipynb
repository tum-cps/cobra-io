{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tolerated Poses Tutorial\n",
    "A key concept of CoBRA is the tolerated pose that defines desired end-effector movements for any robot in a CoBRA task. This tutorial will show you how to use them and highlight their scalability from simple strict nominal poses to more exotic constraints needed for tasks such as drilling or insertion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import timor\n",
    "import cobra"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tolerated Pose Basics\n",
    "<img src=\"figures/SimpleXYConstraint.png\" width=\"500\"/>\n",
    "\n",
    "First, let us consider a pose parallel to the world coordinates at $(x, y, z)$ that allows some variance in $x$ and $y$, denoted by the intervals $[x_{min}, x_{max}]$ and $[y_{min}, y_{max}]$ respectively. The position in the z-direction is not relevant. The CoBRA paper would note this as:\n",
    " * Projections $S(\\mathbf{T}) = [x(\\mathbf{T}), y(\\mathbf{T})]^T$,\n",
    " * Desired Pose$\\mathbf{T}_d = \\begin{bmatrix}\n",
    "1 & 0 & 0 & x \\\\\n",
    "0 & 1 & 0 & y \\\\\n",
    "0 & 0 & 1 & z \\\\\n",
    "0 & 0 & 0 & 1\n",
    "\\end{bmatrix}$, and\n",
    " * interval vector $\\Gamma(\\mathbf{T}_d) = \\begin{bmatrix}\n",
    "x_{min} & x_{max} \\\\\n",
    "y_{min} & y_{max}\n",
    "\\end{bmatrix}$.\n",
    "\n",
    "The pose can be defined in CoBRA and used with Timor as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from colab_utils import display_http\n",
    "from timor.utilities import tolerated_pose, transformation\n",
    "import pinocchio as pin\n",
    "\n",
    "viz = pin.visualize.MeshcatVisualizer()\n",
    "viz.initViewer()\n",
    "display_http(viz.viewer.url())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from timor.utilities.frames import FrameTree\n",
    "from pprint import pprint\n",
    "\n",
    "def vis_tolerated_pose(x, y, z, x_min, x_max, y_min, y_max):\n",
    "  pose = tolerated_pose.ToleratedPose.from_json_data({\n",
    "      \"nominal\": [\n",
    "                [ 1, 0, 0, x ],\n",
    "                [ 0, 1, 0, y ],\n",
    "                [ 0, 0, 1, z ],\n",
    "                [ 0, 0, 0, 1 ]\n",
    "      ],\n",
    "      \"toleranceProjection\": [\n",
    "        \"x\",\n",
    "        \"y\"\n",
    "      ],\n",
    "      \"tolerance\": [\n",
    "        [ x_min, x_max ],\n",
    "        [ y_min, y_max ]\n",
    "      ]\n",
    "    }\n",
    "  )\n",
    "  pose.visualize(viz, \"nominal_pose\")\n",
    "  return pose\n",
    "\n",
    "pose = vis_tolerated_pose(x=-1, y=2, z=1, x_min=-.1, x_max=.1, y_min=-.1, y_max=.1)  # Should be visible in 3D Frame above or under URL shown above\n",
    "test_pose = transformation.Transformation.from_translation((-1, 2, 1))  # Feel free to test different positions here\n",
    "print(f\"Is {test_pose} a valid position?: \", pose.valid(test_pose))  # Should be valid\n",
    "test_pose = transformation.Transformation.from_translation((-1.15, 2, 1))  # Feel free to test different positions here\n",
    "print(f\"Is {test_pose} a valid position?: \", pose.valid(test_pose))  # Should be invalid\n",
    "print(\"The CoBRA serialized version of this pose is:\")\n",
    "pprint(pose.to_json_data())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this more complicated setting, a desired pose is now rotated by 90° about the x-axis. You can observe this change in the rendering above. The allowable tolerance is now defined in the frame of the desired pose; by default, the projections are applied to the simple difference between desired and real pose $\\mathbf{T}_d^{-1} \\mathbf{T}$. This can make it less intuitive (the solution, \"frames\", will be shown afterward)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "from timor.utilities.spatial import rotX\n",
    "\n",
    "def vis_tolerated_pose_rotated(x, y, z, x_min, x_max, y_min, y_max):\n",
    "  pose = tolerated_pose.ToleratedPose.from_json_data({\n",
    "      \"nominal\": transformation.Transformation.from_translation((x, y, z)) @ transformation.Transformation(rotX(math.pi / 2)),\n",
    "      \"toleranceProjection\": [\n",
    "        \"x\",\n",
    "        \"y\"\n",
    "      ],\n",
    "      \"tolerance\": [\n",
    "        [ x_min, x_max ],\n",
    "        [ y_min, y_max ]\n",
    "      ]\n",
    "    })\n",
    "  pose.visualize(viz, \"nominal_pose\")\n",
    "  return pose\n",
    "\n",
    "pose = vis_tolerated_pose_rotated(-1, 2, 1, -.1, .1, -.1, .1)  # Should also be visible in 3D Frame above; note that now y (= green) points upwards\n",
    "test_pose = transformation.Transformation.from_translation((-1, 2.2, 1))  # This is in world coordinates -> y in world is -z in nominal\n",
    "print(f\"Is {test_pose} a valid position?: \", pose.valid(test_pose))  # This is valid as the z coordinate of the tolerated pose is not restricted\n",
    "test_pose = transformation.Transformation.from_translation((-1, 2, 1.2))  # This is in world coordinates -> z in world is y in nominal\n",
    "print(f\"Is {test_pose} a valid position?: \", pose.valid(test_pose))  # Invalid as y in nominal is restricted"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Frames\n",
    "Frames are a way to specify tolerances relative to other frames than just the nominal frame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from timor.utilities.frames import Frame, WORLD_FRAME\n",
    "\n",
    "# TODO : At the moment needs specific beta version of Timor\n",
    "frames = {\n",
    "    # \"world\": {  # Reserved keyword for the world frame; has None parent\n",
    "    #     \"transformation\": transformation.Transformation.neutral(), \"parent\": None\n",
    "    # },\n",
    "    # \"nominal\": {},  # Reserved keyword for the nominal frame; cannot be a parent\n",
    "    # \"nominal_parallel_to_world\": {},  # Reserved keyword for frame at nominal frame with orientation parallel to world frame\n",
    "    \"another\": {\n",
    "        \"transformation\": transformation.Transformation.from_translation((0, 0, 1)), \"parent\": \"world\"\n",
    "    }\n",
    "}\n",
    "\n",
    "def vis_tolerated_pose_rotated_with_frame(x, y, z, x_min, x_max, y_min, y_max):\n",
    "  pose = tolerated_pose.ToleratedPose.from_json_data({\n",
    "      \"nominal\": transformation.Transformation.from_translation((x, y, z)) @ transformation.Transformation(rotX(math.pi / 2)),\n",
    "      \"toleranceProjection\": [\n",
    "        \"x\",\n",
    "        \"y\"\n",
    "      ],\n",
    "      \"tolerance\": [\n",
    "        [ x_min, x_max ],\n",
    "        [ y_min, y_max ]\n",
    "      ],\n",
    "      \"toleranceFrame\": [WORLD_FRAME, WORLD_FRAME]\n",
    "    })\n",
    "  pose.visualize(viz, \"nominal_pose\")\n",
    "  return pose\n",
    "\n",
    "# This one uses two frames. x is measured in world frame, y in a frame at the location of nominal with the same orientation as world\n",
    "pose = vis_tolerated_pose_rotated_with_frame(-1, 2, 1, -.1, .1, -.1, .1)\n",
    "test_pose = transformation.Transformation.from_translation((-1, 2, 1))  # This is valid as x still allowed in -1.1 to -.9\n",
    "print(f\"Is {test_pose} a valid position?: \", pose.valid(test_pose))\n",
    "\n",
    "test_pose = transformation.Transformation.from_translation((-1, 2.2, 1))  # This is invalid as y constrained in frame parallel to world frame\n",
    "print(f\"Is {test_pose} a valid position?: \", pose.valid(test_pose))\n",
    "\n",
    "test_pose = transformation.Transformation.from_translation((-1, 2, 1.2))  # This is valid as z world is not constrained\n",
    "print(f\"Is {test_pose} a valid position?: \", pose.valid(test_pose))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example\n",
    "## Drilling at an Angle Example\n",
    "This more elaborate example will exploit more projection functions, additional frames, and tolerances on different scales to show the power of the CoBRA pose definition. Here we imagine that we want to drill into a wall that is at an angle of 45° with the world frame, intersecting the world's x and y-axis at 2m each. To orient the drill properly, the:\n",
    " * z-axis of the pose goal point into the wall. The orientation can be an arbitrary rotation $\\theta_R$ about the z-axis.\n",
    " * drill should start in front of the wall, i.e., within $z_{min} = -5 cm$ to $z_{max} = -1 cm$ from the wall's surface.\n",
    " * height of the goal is rather precise at $z_{world} = 1m \\pm 1mm$.\n",
    " * location in horizontal direction is not so precise at $x_{min | max} = \\pm 1cm$.\n",
    "<img src=\"figures/ComplexFrameConstraint.png\" width=\"500\"/>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from timor.utilities.frames import NOMINAL_FRAME\n",
    "from timor.task import Tolerance\n",
    "from timor.utilities.tolerated_pose import ToleratedPose\n",
    "from timor.task.Goals import At\n",
    "from timor.utilities import spatial\n",
    "from timor import Transformation\n",
    "from timor.Geometry import Box\n",
    "from timor.task.Obstacle import Obstacle\n",
    "from timor.task.Task import Task, TaskHeader\n",
    "\n",
    "# Create a demo task environment\n",
    "pose_wall_center = Transformation.from_roto_translation(\n",
    "    spatial.rotZ(-math.pi / 4)[:3, :3] @ spatial.rotX(math.pi / 2)[:3, :3],\n",
    "    (1, 1, 1))\n",
    "frame_hole_start = Frame(\"drill\",\n",
    "                          Transformation.from_roto_translation(\n",
    "                              spatial.rotZ(-math.pi / 4)[:3, :3] @ spatial.rotX(-math.pi / 2)[:3, :3],\n",
    "                              (.95, .95, 1)),\n",
    "                         WORLD_FRAME)\n",
    "task = Task(TaskHeader(\"tmp\"),\n",
    "            (Obstacle(\"wall\",\n",
    "                      Box({\"x\": 2, \"y\": 2, \"z\": .1},\n",
    "                          pose=pose_wall_center)), ),\n",
    "            (At(\"drill_start\",\n",
    "                ToleratedPose(\n",
    "                    frame_hole_start,\n",
    "                    Tolerance.Composed((\n",
    "                        Tolerance.CartesianXYZ([-0.01, 0.01], [-math.inf, math.inf], [-0.05, -0.01], frame=NOMINAL_FRAME),\n",
    "                        Tolerance.CartesianXYZ([-math.inf, math.inf], [-math.inf, math.inf], [-.001, .001], frame=WORLD_FRAME),\n",
    "                        Tolerance.RotationAxis([-0.001, 0.001], [-0.001, 0.001], [-math.pi, math.pi], frame=NOMINAL_FRAME),\n",
    "                    ))\n",
    "                )), )\n",
    "            )\n",
    "task.visualize(viz)\n",
    "display_http(viz.viewer.url())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test if the pose is valid for some expected possible start poses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nominal_valid = frame_hole_start.in_world_coordinates() @ Transformation.from_translation((0, 0, -.03))\n",
    "goal_pose = task.goals_by_id[\"drill_start\"].goal_pose\n",
    "print(\"Center of valid:\")\n",
    "print(nominal_valid)\n",
    "print(\"Test limits in z local\")\n",
    "print(\"Is valid: \", goal_pose.valid(nominal_valid @ Transformation.from_translation((0, 0, .02))))  # Valid for -.02 to .02\n",
    "print(\"Test limits in z global\")\n",
    "print(\"Is valid: \", goal_pose.valid(Transformation.from_translation((0, 0, -.001)) @ nominal_valid))  # Valid for -.001 to .001\n",
    "print(\"Test limits in x local (horizontal along wall)\")\n",
    "print(\"Is valid: \", goal_pose.valid(nominal_valid @ Transformation.from_translation((0.01, 0, 0))))  # Valid for -.01 to .01\n",
    "print(\"Test orientation limits in z\")\n",
    "print(\"Is valid: \", goal_pose.valid(nominal_valid @ spatial.rotZ(math.pi)))  # Valid for any angle\n",
    "# TODO: Variations xy not yet tolerated"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

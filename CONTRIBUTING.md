# Contributing Guidelines

Thank you for your interest in contributing to CoBRA I/O. We highly appreciate feedback and contributsions such as bug reports, feature requests, new features, correction, or additional documentation.

Before submitting any issues or merge requests, we kindly ask you to read through this document before.

## Issues / Reporting Bugs
We welcome you to use the gitlab issue tracker to submit issues, bugs, or feature requests. Please provide as much information as possible. When submitting a bug report, please provide a code sample to help us reproduce the issue. For feature requests, we ask you to provide examplatory use cases in which the feature requested could be used.
For all issues submitted, please provide:
- A meaningful title
- The exact software version / git hash of CoBRA I/O you are working with
- Modifications you have made that are relevant to the bug
- Anything unusual about your environment

## Pull requests
Contributions via pull requests are highly appreciated. Before submitting a pull request to CoBRA I/O, please ensure that it fulfills the following criteria:
- Atomic: One pull request should resolve exactly one issue / implement one feature. Use multiple pull requests to implement multiple feautres / bugfixes.
- Documented: The pull request has a meaningful title, methods, classes and types implementet come with in-code documentation, and further information necessary to understand the changes implemented is provided using the gitlab comment feature.
- Tested: The test cases pass for the branch requested to merge. For every new feature implemented, write a unittest that ensures it is working as intended.
- Reviewed: A review for the pull request has been requested.

## License
Any contribution to CoBRA I/O python will be under the MIT license.

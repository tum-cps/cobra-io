from typing import Any, Dict

import meshcat
from IPython.display import display, HTML
from html import escape


def remote_meshcat_vis(viewer: meshcat.Visualizer):
    """Utility to render tasks / solutions with notebook running on remote server"""
    if hasattr(viewer, "viewer"):
        viewer = viewer.viewer
    viewer_html = escape(viewer.static_html())
    iframe = HTML(f"<iframe srcdoc=\"{viewer_html}\" height=800px width=1000px/>")
    display(iframe)


def display_cobra(cobra_object: Dict[str, Any]):
    """Show html for an object retrieved from the cobra website."""
    if 'html' not in cobra_object:
        raise ValueError("Object does not have an html attribute.")
    display_http(cobra_object['html'])


def display_http(url: str):
    """Show html for a url."""
    iframe = HTML(f"<iframe src=\"{url}\" height=800px width=1000px/>")
    display(iframe)

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Solution Details\n",
    "\n",
    "This Notebook will show you more details on how to handle CoBRA solutions. We will deeply dive into the returns of the `find_` functions and how to retrieve a specific solution. We also show how a solution is constructed and may be altered.\n",
    "\n",
    "First, let us find some interesting solutions for the task `simple/PTP_1` and the cost function cycle time, abbreviated by `cyc`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cobra.solution.solution import find_solutions\n",
    "solution_uuids, solution_details = find_solutions(task_id='simple/PTP_1', cost_function='cyc')\n",
    "print(\"Found {} solutions.\".format(len(solution_uuids)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solutions within the CoBRA benchmark are only identifiable via their assigned UUID. But `find_solutions` provides a unified query interface to find all solutions for a specific task, cost function, etc.\n",
    "Now let's take a look at which information CoBRA provides for each solution:\n",
    "* `id`: The UUID of the solution.\n",
    "* `scenario_id`: The UUID of the task this solution is meant for. It could be used with `cobra.task.get_task(uuid=scenario_id)`.\n",
    "* `scenario_name`: The name of the task this solution is meant for.\n",
    "* `version`: The version of CoBRA this solution was generated for.\n",
    "* `costFunction`: The cost function used to evaluate the solution. A complete list is given in [Sec. 4 of the Solution Documentation](https://cobra.cps.cit.tum.de/crok-documentation/solution).\n",
    "* `cost`: The cost of the solution, here that would be a cycle time in seconds.\n",
    "* `user`: The user who submitted the solution.\n",
    "* `json`: A link to access the JSON file of the solution.\n",
    "* `html`: A link to access the HTML visualization of the solution.\n",
    "* `png`: A link to access an image of the solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solution_details[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may now for example take a closer look at the solution by downloading its visualization:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from colab_utils import display_cobra\n",
    "display_cobra(solution_details[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solution retrieval\n",
    "Now that we have a specific solution of interest, we can download it via its UUID. The `get_solution` function is handy as it will also gather the associated task file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cobra.solution.solution import get_solution\n",
    "\n",
    "solution_file, task_file = get_solution(solution_uuids[0])\n",
    "print(solution_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar to tasks, solutions are just simple json files. We can load them into a python dictionary and inspect them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pprint import pprint\n",
    "import json\n",
    "solution_data = json.load(open(solution_file))\n",
    "pprint(solution_data.keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solutions contain similar fields as tasks describing the meta-data such as `date`, `author`, `email`, and `affiliation`. Additionally, they refer to a CoBRA `version` and task via `taskID`.\n",
    "The employed robot and its prescribed trajectory describe the solution itself (together with some additional metadata). The `moduleSet`, `moduleOrder`, and `basePose` define the robot. The module set and order have already been shown in the URDF generation in [Robot_Details.ipynb](Robot_Details.ipynb). The base pose states where the robot is placed with a $4 \\times 4$ homogenous transformation matrix.\n",
    "The `trajectory` is a bit more complex."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pprint(solution_data['trajectory'].keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A trajectory describes the robot's movement over time `t`. The movement is defined via samples of the robot position vector $q$, velocity $\\dot{q}$, and acceleration $\\ddot{q}$. Additionally, the `goal2time` mapping tells which goal from the task is solved at which time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "print(\"goal2time:\")\n",
    "pprint(solution_data['trajectory']['goal2time'])\n",
    "first_goal_id, first_goal_time = tuple(solution_data['trajectory']['goal2time'].items())[0]\n",
    "t = np.asarray(solution_data['trajectory']['t'])\n",
    "time_idx = np.argwhere(t == first_goal_time)[0, 0]\n",
    "print(f\"State solving goal q = {solution_data['trajectory']['q'][time_idx]} @ t = {first_goal_time} sec.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above, we extracted the robot state the solution claims solves goal '1' at time t = 1.0 seconds. This is verified when submitting a solution to CoBRA.\n",
    "\n",
    "Solution submission was already covered in the [Basic Tutorial](0_CoBRA_Introduction.ipynb). Here we will show how to delete a solution. This can be done via the cobra frontend (https://cobra.cps.cit.tum.de/user/submissions) or the `delete_solution` function. This will fail if you are not the owner of the solution. If you want to try this, please upload your solution first, as shown in the basic tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cobra.solution.solution import delete_solution\n",
    "delete_solution(solution_uuids[0], user_email=\"your-email\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solution Editing\n",
    "For solutions, we also provide a JSON schema to evaluate their validity. For the downloaded solution, we can check this via:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cobra.solution.solution import get_schema_validator\n",
    "schema, validator = get_schema_validator()\n",
    "validator.is_valid(solution_data)  # The unedited solution is valid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Edits are as easy as in the case of tasks and Modules. Just alter the loaded dictionary in Python, the parsed JSON data in your chosen language, or even a simple text editor. To check edits, pass them to the schema validator. Some edits might not work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solution_data.pop('taskID')  # Removing the taskID will make the solution invalid.\n",
    "validator.is_valid(solution_data)"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "authorship_tag": "ABX9TyMFaCPj87P6MECtSIuV1OQb",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

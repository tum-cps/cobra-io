{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Robots\n",
    "All robots supported by the benchmark can be accessed via the CoBRA API. Our website offers the robot data's current [complete specification](https://cobra.cps.cit.tum.de/crok-documentation/robot). To list all available robots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cobra.robot\n",
    "robot_list = cobra.robot.get_available_module_dbs()\n",
    "print(robot_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "module_db_file = cobra.robot.get_module_db('IMPROV')\n",
    "print(\"A module database is primarily a json file\", module_db_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Robot Model Overview\n",
    "Sets of the robot modules $\\mathcal{M}$ can be retrieved as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pprint import pprint\n",
    "import json\n",
    "module_db = json.load(module_db_file.open(\"r\"))\n",
    "pprint(module_db.keys())  # More complex database can contain additional information, e.g., regarding inter-module collision checking.\n",
    "pprint(module_db['modules'][0].keys())  # Each module is a dictionary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us take a closer look at single modules. We will start with a single, I-shaped link module without a joint. Similar to tasks, each module in a database contains a header with author information, a unique ID, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m_I_shaped = next(m for m in module_db['modules'] if m[\"header\"][\"ID\"] == \"7\")\n",
    "pprint(m_I_shaped['header'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This module has no joints, so the primary information is in the body's dictionary. Also, each body has a unique ID within the module (here, `7`). The dynamic properties are covered by the body's mass $m$, center of gravity relative to the body frame $r_{CoM}$, and inertia tensor $\\mathbf{I}$. Its geometry is covered by the 'collision' field encoding the set of collision points of the body $\\mathcal{C}$ and (optional) the 'visual' field encoding the set of visual points $\\mathcal{V}$. Lastly, each module has connectors that define where the module can be attached to other modules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pprint(m_I_shaped['bodies'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A last remark should regard the connector. It specifies a mechanical interface to other modules, which describes other fitting modules via `gender`, a list of supported `size`s, and a `type`. Additionally, it contains a `pose` that describes the relative pose of the connector to the module's body frame. The following end-effector module would fit onto the I-shaped module (`7_distal_connector` has opposing gender and a common type and size with `12_proximal_connector`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "next(m for m in module_db['modules'] if m[\"header\"][\"ID\"] == \"12\")['bodies'][0]['connectors']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly, let us look at a joint module, e.g., module '1'. It is a rather complex module to show the flexibility of CoBRA's format. The module contains three bodies and two joints."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m_joint = next(m for m in module_db['modules'] if m[\"header\"][\"ID\"] == \"21\")\n",
    "pprint(len(m_joint['bodies']))\n",
    "pprint(len(m_joint['joints']))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Bodies from different modules can connect via connectors, as previously stated. Bodies in the same module are connected via joints in their `parent` and `child` fields. This module seems to be a chain of body_1 -> joint_1 -> body_2 -> joint_2 -> body_3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for j in m_joint['joints']:\n",
    "    print(j['ID'], ' : ', j['parent'], j['child'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The joints additionally define\n",
    "  * transformations from each body to the joint frame (`poseParent` $T_p$, `poseChild` $T_c$),\n",
    "  * a joint type (such as `revolute` or `prismatic`),\n",
    "  * joint limits (upper $q_{max}$, lower $q_{min}$, maximum absolute torque $\\tau_{max}$, velocity $\\dot{q}_{max}$, and acceleration $\\ddot{q}_{max}$), and\n",
    "  * dynamic properties (viscous friction $f_v$, Coulomb friction $f_c$, rotor inertia $I_m$, and if the joint is passive/actuated)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pprint(m_joint['joints'][0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CoBRA and URDF\n",
    "The CoBRA API can also be used to generate URDF files from a given assembly of modules. To use it with CoBRA I/O, follow this code snippet:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cobra.robot.robot import get_urdf\n",
    "example_urdf = get_urdf(\"IMPROV\", ['1', '21', '4', '21', '5', '23', '7', '12'])  # You can alter this, e.g., remove the second to last module"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please feel free to experiment with different module databases and module IDs. Ensure you respect the connector fit when choosing modules, as this is enforced during URDF generation. The `IMPROV` set is relatively forgiving as it has a single connector type and size.\n",
    "\n",
    "As this is a standard URDF file, it can be loaded with many libraries, such as Pinocchio:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pinocchio as pin\n",
    "IMPROV_robot = pin.RobotWrapper.BuildFromURDF(str(example_urdf), str(example_urdf.parent))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pinocchio can be used to compute the forward kinematics of the robot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "pin.forwardKinematics(IMPROV_robot.model, IMPROV_robot.data, np.zeros((6,)))  # Show kinematic calculations\n",
    "print(IMPROV_robot.data.oMi[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pinocchio has also some powerful visualization tools:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from colab_utils import remote_meshcat_vis\n",
    "\n",
    "visualizer = pin.visualize.MeshcatVisualizer(\n",
    "    IMPROV_robot.model, IMPROV_robot.collision_model, IMPROV_robot.visual_model,\n",
    "    copy_models=False,\n",
    "    data=IMPROV_robot.data,\n",
    "    visual_data=IMPROV_robot.visual_data)\n",
    "visualizer.initViewer()\n",
    "visualizer.loadViewerModel(rootNodeName=IMPROV_robot.model.name)\n",
    "remote_meshcat_vis(visualizer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Final Remarks\n",
    "Thank you for taking the time to learn more about CoBRA's robot description format. In this tutorial, you learned how to find available robot module databases, read them and the individual modules, and generate a URDF file for a specific robot assembly.\n",
    "We want to highlight that most of this functionality is implemented in [timor-python](https://pypi.org/project/timor-python/), which enables local URDF generation and robot simulation. To create your own module dataset, you can edit the provided module databases, and we encourage you to start with the simplest `geometric_primitive_modules`. Similar to tasks, we provide a module schema to check whether your edits are CoBRA compliant (see [Task_API, Section Task Schema](Task_API.ipynb))."
   ]
  }
 ],
 "metadata": {
  "colab": {
   "authorship_tag": "ABX9TyPUXD4xF4/9Hska4QxJDM27",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

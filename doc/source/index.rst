Welcome to the CoBRA I/O documentation!
=======================================

Version: |version|

Introduction
------------
Welcome to the documentation of CoBRA I/O - the python interface to the **Co**\ mposable **B**\ enchmark for **R**\ obotics **A**\ pplications.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   autoapi/index


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Important references
--------------------

* `Gitlab Repository <https://gitlab.lrz.de/tum-cps/cobra-io>`_
* `CoBRA Benchmark with descriptions for Robots, Scenarios, Solutions, Costs <https://cobra.cps.cit.tum.de/crok-documentation/robot>`_

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Welcome to the CoBRA - Timor Integration Tutorial\n",
    "\n",
    "This Tutorial will show you how [CoBRA](https://cobra.cps.cit.tum.de/), the **Co**llaborative **B**enchmark for **R**obotics **A**pplications interacts with [Timor Python](https://pypi.org/project/timor-python/), the **T**oolbox for **I**ndustrial **Mo**dular **R**obotics.\n",
    "We will show you the following:\n",
    "\n",
    "* How to download tasks from CoBRA and visualize them with timor.\n",
    "* How to retrieve models of Modular Robots and load them with timor.\n",
    "* How to create a solution with timor and upload it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# pinocchio needs conda within colab.\n",
    "# In a local jupyter notebook you can skip this cell\n",
    "!pip install -q condacolab\n",
    "import condacolab\n",
    "condacolab.install()\n",
    "!conda install pinocchio -c conda-forge"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install cobra-io.[full] timor-python  # Install cobra-io to interface benchmark and timor as an example tool to work with the benchmark"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Retrieve Task Files and Assets\n",
    "\n",
    "As in the introductory tutorial, we download tasks via the CoBRA API using CoBRA IO. Here we will use Timor's task object to visualize the robot's appointment. Please look at the [task tutorial](Task_Details.ipynb) to understand and edit tasks more precisely."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cobra.task\n",
    "task_file = cobra.task.get_task(id=\"simple/PTP_3\", version=\"2022\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display task\n",
    "from timor.task import Task\n",
    "from colab_utils import remote_meshcat_vis\n",
    "task = Task.Task.from_json_file(task_file)\n",
    "remote_meshcat_vis(task.visualize())  # If run locally you can skip the remote_meshcat_vis wrapper"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Solve a Task with Timor\n",
    "First, we have to get a robot model from CoBRA. As we simulate it with timor, we can use a prebuilt 6-axis robot. If you want to alter or change this robot, look at the [robot tutorial](Robot_Details.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from timor.utilities import prebuilt_robots\n",
    "from timor.task import Solution, CostFunctions\n",
    "from timor.utilities.trajectory import Trajectory\n",
    "import numpy as np\n",
    "\n",
    "robot_assembly = prebuilt_robots.get_six_axis_assembly()\n",
    "robot = robot_assembly.robot\n",
    "robot.set_base_placement(task.base_constraint.base_pose.nominal)  # Set base pose to nominal pose of the task\n",
    "v = task.visualize()\n",
    "robot.visualize(v)\n",
    "remote_meshcat_vis(v)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now create a solution for this task. Note that this task does not need a tightly sampled trajectory but only needs to pass through the desired goal poses. Therefore, the solution strategy is to solve the inverse kinematics (IK) for each goal pose and then concatenate it to a solution movement. This simple algorithm may only satisfy some task constraints, which we test in the next step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_ik(task, robot):\n",
    "    sol = {}\n",
    "    for id, goal in task.goals_by_id.items():\n",
    "        valid = False\n",
    "        while not valid:\n",
    "            q, valid = robot.ik(goal.goal_pose, task=task)\n",
    "            if valid:\n",
    "                sol[id] = q\n",
    "                print(f\"Solve goal {id} with q={q}\")\n",
    "\n",
    "    return sol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Timor provides a solution class that interacts with CoBRA. We will now construct such an object for easy submission to the benchmark. To edit solutions more precisely, refer to the [solution tutorial](Solution_Details.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "while True:\n",
    "    sol = solve_ik(task, robot)\n",
    "    qs = np.asarray(list(sol.values()))\n",
    "    qs = np.repeat(qs, 2, axis=0)\n",
    "    solution_trajectory = Trajectory(\n",
    "        t=(0., 0.99, 1., 1.99, 2., 2.99),  # Stay at each goal pose for a second\n",
    "        q=qs, dq=np.zeros_like(qs), ddq=np.zeros_like(qs),  # Set joint angles to those solving the IK problem\n",
    "        goal2time={\"1\": 0., \"2\": 1., \"3\": 2.})  # Tell when each goal fulfilled\n",
    "    solution = Solution.SolutionTrajectory(\n",
    "        solution_trajectory,\n",
    "        Solution.SolutionHeader(\n",
    "            task.id,\n",
    "            author=['your-name'],\n",
    "            email=['your-email'],  # For submission to the benchmark website include your login email here\n",
    "            version=\"2022\"),\n",
    "        task=task,\n",
    "        assembly=robot_assembly,\n",
    "        cost_function=CostFunctions.NumJoints(0.025) + CostFunctions.RobotMass(0.1)  # Intoduced by Whitman et al., Modular Robot Design Synthesis with Deep Reinforcement Learning, 2020\n",
    "    )\n",
    "\n",
    "    if not solution.valid:\n",
    "        print(\"Rerun as solution failed\")\n",
    "        for g in solution.task.goals:\n",
    "            if not g.achieved(solution):\n",
    "                print(f\"Goal {g.id} not achieved\")\n",
    "        for c in solution.task.constraints:\n",
    "            if not c.fulfilled(solution):\n",
    "                print(f\"Constrained {type(c)} not fulfilled\")\n",
    "    else:\n",
    "        print(\"Valid solution found\")\n",
    "        remote_meshcat_vis(solution.visualize())\n",
    "        break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Submit Solution\n",
    "Finally, let us submit the found solution. [Register an account](https://cobra.cps.cit.tum.de/signup) and use your registration E-Mail in the solution created in the previous section."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cobra.solution\n",
    "import tempfile\n",
    "from pathlib import Path\n",
    "with tempfile.NamedTemporaryFile(suffix=\".json\") as solution_file:\n",
    "    solution.to_json_file(solution_file.name)\n",
    "    submission_success = cobra.solution.submit_solution(solution_file.name, solution.header.email[0])\n",
    "if submission_success:\n",
    "    print(\"Submitted solution successfully. Find it at cobra.cps.cit.tum.de/user/submissions\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conclusion\n",
    "\n",
    "Thank you for being so interested in the CoBRA benchmarking suite. This second tutorial covered how to interact with the CoBRA API to retrieve robot models and tasks, load them into Timor Python, and create a solution to a simple task and upload it.\n",
    "\n",
    "As the following steps, we suggest looking into the advanced tutorial in this folder or on [CoBRA](https://cobra.cps.cit.tum.de/not-found)  to take a closer look at each element of the CoBRA benchmark suite."
   ]
  }
 ],
 "metadata": {
  "colab": {
   "authorship_tag": "ABX9TyNj2MscGTurgfvqO/2BuG7k",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
